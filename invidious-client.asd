(asdf:defsystem #:invidious-client
  :description "Describe invidious-client here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("invidious-client.api" "safe-queue" "blackbird" "feeder" "cl-store")
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "resource-owner")
                         (:file "lazy-load-mixin")
                         (:file "channel")
                         (:file "video")
                         (:file "content-source")
                         (:file "backup-channel")
                         (:file "client")
                         (:file "subscription-collection")
                         (:file "client-storage")
                         (:file "video-player")))))
