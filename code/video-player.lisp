#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass video-player () ())

;;; might be better to involve the client and allow the player
;;; to chose the best content source?
(defgeneric play-video (video-player client video-url))

(defclass mpv (video-player) ())

(defmethod play-video ((video-player mpv) (client client) video)
  (let ((video-url (make-video-url (first (content-sources client)) video)))
    (uiop:run-program (list "mpv" video-url))))
