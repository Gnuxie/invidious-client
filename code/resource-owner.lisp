#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defun get-thumbnail-data (thumbnail)
  (flet ((thumbnail-thunk ()
           (multiple-value-bind (data status headers)
               (drakma:http-request (url thumbnail))
             (declare (ignore status))
             (let ((content-type (cdr (assoc :content-type headers))))
               (assert (not (null content-type)))
               (list data content-type)))))
    (ip:send ipw:*resource-loader-tube*
             (make-instance 'ipw:api-call
                            :function #'thumbnail-thunk
                            :request-uri (url thumbnail)
                            :route-name "thumbnail"))))

(defun get-resource (uri)
  (ip:send ipw:*resource-loader-tube*
           (make-instance 'ipw:api-call
                          :function (lambda () (drakma:http-request uri))
                          :request-uri uri
                          :route-name "resource")))
