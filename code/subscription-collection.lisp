#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass subscription-collection ()
  ((%channels :initarg :channels :reader channels
              :initform (make-array 1 :fill-pointer 0 :adjustable t))))

(defgeneric subscribe (subscription-collection channel)
  (:method ((subscription-collection subscription-collection)
            (channel simple-channel))
    (let ((existing-entry (find (author-id channel) (channels subscription-collection)
                                :test #'string= :key #'author-id)))
      (unless existing-entry
        (vector-push-extend channel (channels subscription-collection))))))

(defgeneric subscribed-videos (client &key thumbnail-loaded-callback)
  (:method ((client client) &key thumbnail-loaded-callback)
    (bb:alet ((unsorted-video-lists
               (bb:all
                (loop :for channel :across (channels (subscriptions client))
                   :collect (channel-videos-with-atom-as-backup client
                                              (author-id channel)
                                              :thumbnail-loaded-callback
                                              thumbnail-loaded-callback)))))
      (let ((unsorted-videos (apply #'append unsorted-video-lists)))
        (sort unsorted-videos #'local-time:timestamp> :key #'published)))))
