#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass content-source () ())

(defgeneric make-video-url (content-source video))

(defclass youtube (content-source) ())

(defmethod make-video-url ((content-source youtube) (video video))
  (format nil "https://www.youtube.com/watch?v=~a" (video-id video)))
