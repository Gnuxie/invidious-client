#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass video ()
  ((%title :initarg :title :reader title :type string)
   (%video-id :initarg :video-id :reader video-id :type string)
   (%published :initarg :published :reader published :type local-time:timestamp)
   (%channel :initarg :channel :reader channel))
  (:documentation "This is a protocol class, it's supposed to have the minimum
amount of information needed for a video and thus presentation to be playable." ))

;;; so i have this idea that for lazy loading images we have this
;;; image class or some lazy loaded property mixin
;;; this basically has a 'ready' boolean whether it's ready or not
;;; and the thing that's fetching the resources will set it back
;;; to ready and force repaint.

(defclass detailed-video (video)
  ((%video-length :initarg :video-length :reader video-length
                  :type integer)
   (%view-count :initarg :view-count :reader view-count
                :type integer)
   (%thumbnail :initarg :thumbnail :reader thumbnail
               :type thumbnail)))
;;; flexi-streams:with-input-from-sequence

(defvar *default-thumbnail-data*)

(defclass thumbnail (lazy-resource)
  ((%url :initarg :url :reader url :type string)
   (%width :initarg :width :reader width :type integer
           :initform 120)
   (%height :initarg :height :reader height :type integer
            :initform 90)
   (%data :initarg :data :accessor data
          :initform *default-thumbnail-data*)))

(defgeneric update-thumbnail-with-data (thumbnail data content-type)
  (:documentation "This is used to update a thumbnail instance
with new raw data for it's thumbnail that has just been downloaded."))


;;; should update-thumbnail-with-data *really* be seperate to
;;; whatever hooks onto retrieve data?
;;; i don't think so buddy.
(defgeneric retrieve-data (thumbnail)
  (:documentation "We deliberatly leak the promise here
but i'm not sure this is a good idea.")
  (:method ((thumbnail thumbnail))
    (bb:attach
     (get-thumbnail-data thumbnail)
     (lambda (data)
       (update-thumbnail-with-data thumbnail (first data) (second data))
       data))))

(defun select-thumbnail (video-thumbnails)
  (find-if (lambda (thumbnail)
             (if (jsown:keyp thumbnail "quality")
                 (string= (jsown:val-safe thumbnail "quality") "default")
                 (> 1200 (jsown:val thumbnail "width"))))
           video-thumbnails))

(defun make-video-from-result (result)
  (make-instance 'detailed-video
                 :title (jsown:val result "title")
                 :video-id (jsown:val result "videoId")
                 :video-length (jsown:val result "lengthSeconds")
                 :view-count (jsown:val result "viewCount")
                 :published (local-time:unix-to-timestamp (jsown:val result "published"))
                 :thumbnail
                 (let ((thumbnail-jsown
                        (select-thumbnail (jsown:val result "videoThumbnails"))))
                   (make-instance 'thumbnail
                                  :url (jsown:val thumbnail-jsown "url")
                                  :width (jsown:val thumbnail-jsown "width")
                                  :height (jsown:val thumbnail-jsown "height")))
                 :channel
                 (make-instance 'simple-channel
                                :author (jsown:val result "author")
                                :author-id (jsown:val result "authorId")
                                :author-url (jsown:val result "authorUrl"))))
