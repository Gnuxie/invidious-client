#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

;;; should something like the channel-mould
;;; have a channel that's immutable
;;; then with each channel you make a new pane
;;; active-mould then should return an accessor
;;; that we funcall on the cast-pane to get the active mould.
(defclass sourcing-cast-pane (cast-pane)
  ((%active-mould-accessor :initarg :active-mould-accessor
                  :accessor active-mould-accessor
                  :type (or null function)
                  :initform nil)

   (%search-mould :initarg :search-mould
                  :accessor search-mould
                  :initform (make-instance 'search-mould))

   (%channel-mould :initarg :channel-mould
                   :accessor channel-mould
                   :initform nil)
   (%subscriptions-mould :initarg :subscriptions-mould
                         :accessor subscriptions-mould
                         :initform nil)))

(defmethod active-mould ((pane sourcing-cast-pane))
  (with-accessors ((active-mould-accessor active-mould-accessor)) pane
    (when active-mould-accessor
      (funcall active-mould-accessor pane))))
