#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

;;; we assume there will be some kind of header (like for a channel
;;; video for comments, and what you're searching for and general settings
;;; when using search
(defclass pagination-mould (mould)
  ((%items :initarg :items
           :reader items
           :initform (make-array 1 :fill-pointer 0 :adjustable t))))

(defmethod mould-pane ((mould pagination-mould) pane)
  (when (< 0 (length (items mould)))
    (formatting-table (pane :y-spacing 5)
      (formatting-column (pane)
        (loop :for item :across (items mould)
           :do (formatting-cell (pane)
                 (present item (presentation-type-of item) :stream pane)))))))

(defgeneric add-items (pagination-mould items)
  (:method ((pagination-mould pagination-mould) (items list))
    (loop :for item :in items
       :do (vector-push-extend item (items pagination-mould)))))
