#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

(defclass cast-pane (clim:clim-stream-pane)
  ())

(defgeneric active-mould (cast-pane))

(defclass mould () ())

(defgeneric mould-pane (mould stream)
  (:documentation "This is how mould's should implement their display-methods."))

(defgeneric display-cast-pane (frame pane)
  (:method (frame (pane cast-pane))
    (when (active-mould pane)
      (mould-pane (active-mould pane) pane))))

;;; because of async, wouldn't a cast pane need to have some kind of ticket system
;;; whereby new events have to match the current ticket for the mould
;;; or they get rejected?
