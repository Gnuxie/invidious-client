#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

(defun queue-invidious-event (frame event)
  (let ((top-level-sheet (frame-top-level-sheet frame)))
    (queue-event top-level-sheet event)))

(defclass invidious-client-event (clim:window-manager-event)
  ((%sheet :initarg :sheet :reader clim:event-sheet)))

(defclass redisplay-frame-pane-event (invidious-client-event)
  ((%frame-pane :initarg :frame-pane :reader frame-pane)))

(defun redisplay-frame-pane-event (frame pane)
  (queue-invidious-event
   frame (make-instance 'redisplay-frame-pane-event
                        :sheet frame
                        :frame-pane pane)))

(defun thumbnail-load-event (frame)
  (redisplay-frame-pane-event frame (get-frame-pane frame 'sourcing-cast-pane)))

(defclass pagination-items-event (invidious-client-event)
  ((%items :initarg :items :reader items)
   (%pagination-mould :initarg :pagination-mould :reader pagination-mould)
   (%cast-pane :initarg :cast-pane :reader cast-pane)))

(defun pagination-items-event (frame items pagination-mould cast-pane)
  (queue-invidious-event frame
                         (make-instance 'pagination-items-event
                                        :items items
                                        :pagination-mould pagination-mould
                                        :cast-pane cast-pane
                                        :sheet frame)))

(defclass tube-event (invidious-client-event)
  ((%event :initarg :event :reader event)))

(defun tube-event (frame event)
  (queue-invidious-event
   frame (make-instance 'tube-event :event event :sheet frame)))
