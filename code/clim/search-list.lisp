#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

(defvar *threads-started-p* nil)
;;; why can't we put this in the tube somehow instead?
;;; we also need to think about getting hold of the
;;; api-tube and making a subclass to be instnatiated
;;; so that other users of the wrapper
;;; aren't clobbered by our event handlers
(defvar *invidious-client-frame* nil)

(setf client:*default-thumbnail-data*
      (clim:make-pattern-from-bitmap-file
       (asdf:system-relative-pathname
        '#:invidious-client "resource/no-thumbnail-small.png")
       :format :png))

(defun format-seconds (stream seconds)
  (multiple-value-bind (minutes seconds) (floor seconds 60)
    (format stream "~D:~D" minutes seconds)))

(defun load-client ()
  (let ((existing-client (client:load-client)))
    (if existing-client
        existing-client
        (make-instance 'client:client
                       :invidious-sources
                       (list
                        (make-instance 'rest-api-description.authentication::authentication
                                       :origin "https://invidio.us"))
                       :content-sources
                       (list
                        (make-instance 'client:youtube))
                       :video-player (make-instance 'client:mpv)))))

(clim:define-presentation-method clim:present (video (type client:video)
                                                     (stream clim:extended-output-stream)
                                                     view &key)
  (declare (ignore view))
  (formatting-table (stream :x-spacing 5)
    (formatting-row (stream)
      (formatting-cell (stream)
        (multiple-value-bind (x y) (stream-cursor-position stream)
          (draw-pattern* stream client:*default-thumbnail-data* x y)))
      (formatting-cell (stream :align-y :top)
        (format stream "~a~%" (client:title video))
        (present (client:channel video) 'client:simple-channel :stream stream)
        (format stream "~%~a~%" (local-time:format-timestring nil
                                                              (client:published video)
                                                              :format local-time:+asctime-format+))))))

(define-presentation-method present (video (type client:detailed-video)
                                           (stream extended-output-stream) view &key)
  (declare (ignore view))
  (with-output-as-presentation (stream video 'client:detailed-video :single-box t)
    (formatting-table (stream :x-spacing 5)
      (formatting-row (stream)
        (formatting-cell (stream)
          (multiple-value-bind (x y) (stream-cursor-position stream)
            (draw-pattern* stream (client:data (client:thumbnail video)) x y)))
        (formatting-cell (stream :align-y :top)
          (format stream "~a~%" (client:title video))
          (present (client:channel video) 'client:simple-channel :stream stream)
          (format stream "~&~:D view~:P~%" (client:view-count video))
          (format stream "~a~%" (local-time:format-timestring nil
                                                              (client:published video)
                                                              :format local-time:+asctime-format+))
          (format-seconds stream (client:video-length video)))))))

(define-presentation-method present (channel (type client:simple-channel)
                                             stream view &key)
  (declare (ignore view))
  (with-output-as-presentation (stream channel 'client:simple-channel)
    (write-string (client:author channel) stream)))

(define-application-frame invidious-client ()
  ((%client :initform (load-client)
            :reader client)
   (%search-results :initform nil :accessor search-results)
   (interaction-pane)
   (sourcing-cast-pane))
  (:pointer-documentation t)
  (:geometry :width 1080 :height 720)
  (:panes (interactor :interactor)
          (sourcing-cast-pane
           (make-pane 'sourcing-cast-pane
                      :display-function 'display-cast-pane))
          (thread-monitor
           (make-pane 'ip.c:thread-monitor
                      :display-function 'ip.c:display-thread-monitor)))
  (:layouts (default
                (vertically ()
                  (3/4
                   (scrolling (:scroll-bars :vertical)
                     sourcing-cast-pane))
                  (1/4
                   (vertically ()
                     (1/2
                      (labelling (:label "Thread Status")
                        thread-monitor))
                     (1/2 interactor)))))))

(define-invidious-client-command (com-search :menu "Search" :name "Search")
    ((query string))
  (let* ((frame *application-frame*)
         (sourcing-cast-pane (get-frame-pane frame 'sourcing-cast-pane)))
    ;; it's important to make a new mould after each command so that
    ;; old material and references are removed and no longer relevant to the
    ;; current search.
    (setf (search-mould sourcing-cast-pane) (make-instance 'search-mould))
    (setf (active-mould-accessor sourcing-cast-pane) #'search-mould)
    (bb:alet ((videos
               (client:youtube-search
                (client *application-frame*)
                query
                :thumbnail-loaded-callback
                (lambda (thumbnail)
                  (declare (ignore thumbnail))
                  (thumbnail-load-event frame)))))
      (pagination-items-event frame videos (search-mould sourcing-cast-pane)
                              sourcing-cast-pane))
    nil))

(define-invidious-client-command (com-play-video :name "Play Video")
    ((video client:video :gesture :select))
  (let ((client (client *application-frame*)))
    (play-video client video *application-frame*))
  nil)

(define-invidious-client-command (com-show-subscriptions :menu "Subscriptions"
                                                         :name "Subscriptions")
    ()
  (let* ((frame *application-frame*)
         (client (client frame))
         (sourcing-cast-pane (get-frame-pane frame 'sourcing-cast-pane)))
    (setf (subscriptions-mould sourcing-cast-pane)
          (make-instance 'subscriptions-mould))
    (setf (active-mould-accessor sourcing-cast-pane) #'subscriptions-mould)
    (bb:alet ((videos (client:subscribed-videos client
                                                 :thumbnail-loaded-callback
                                                 (lambda (thumbnail)
                                                   (declare (ignore thumbnail))
                                                   (thumbnail-load-event frame)))))
      (pagination-items-event frame videos (subscriptions-mould sourcing-cast-pane)
                              sourcing-cast-pane))
    nil))

(define-invidious-client-command (com-show-channel :name "Show Channel")
    ((channel client:simple-channel :gesture :select))
  ()
  (let* ((frame *application-frame*)
         (sourcing-cast-pane (get-frame-pane frame 'sourcing-cast-pane)))
    (flet ((thumbnail-event-callback (thumbnail)
             (declare (ignore thumbnail))
             (thumbnail-load-event frame)))
      (setf (channel-mould sourcing-cast-pane)
            (make-channel-mould 'channel-mould :channel channel))
      (setf (active-mould-accessor sourcing-cast-pane) #'channel-mould)
      (bb:alet ((detailed-channel
                 (client:channel-detail
                  (client frame) (client:author-id channel)
                  :banner-load-callback #'thumbnail-event-callback
                  :thumbnail-load-callback #'thumbnail-event-callback)))
        (queue-invidious-event frame (make-instance 'channel-detail-event
                                                    :channel detailed-channel
                                                    :sheet frame))))
    (bb:alet ((videos (client:channel-videos-with-atom-as-backup
                       (client frame) (client:author-id channel)
                       :thumbnail-loaded-callback
                       (lambda (thumbnail)
                         (declare (ignore thumbnail))
                         (thumbnail-load-event frame)))))
      (pagination-items-event frame videos
                              (channel-mould sourcing-cast-pane)
                              sourcing-cast-pane))
    nil))

(define-invidious-client-command (com-subscribe)
    ((channel client:simple-channel :gesture :edit))
  ()
  (let ((client (client *application-frame*)))
    (client:subscribe (client:subscriptions client) channel)
    (client:save-client client)))

(defun initialize-threads (frame)
  (setf blackbird:*debug-on-error* t)
  (dotimes (i (client:resource-loader-thread-count (client frame)))
    (ip:spawn-thread infernal-pneumatic-wraps:*resource-loader-tube*
                     :special-bindings `((*invidious-client-frame* . ,*invidious-client-frame*))))
  (setf *threads-started-p* t))

(defun start ()
  (pushnew (cons "application" "json") drakma:*text-content-types*
           :test #'equal)
  (let ((frame (clim:make-application-frame
                'invidious-client.clim::invidious-client)))
    (bt:make-thread (lambda () (clim:run-frame-top-level frame)))
    (setf *invidious-client-frame* frame)
    (sleep 0.2)
    (unless *threads-started-p*
      (initialize-threads frame))
    frame))

(defmethod client:update-thumbnail-with-data ((thumbnail client:thumbnail) data
                                               content-type)
  (let ((opticl-fun
         (alexandria:switch (content-type :test 'string=)
           ("image/jpeg" #'opticl:read-jpeg-stream)
           ("image/png" #'opticl:read-png-stream)
           (t #'opticl:read-image-stream))))
    (setf (client:data thumbnail)
          (flexi-streams:with-input-from-sequence (stream data)
            (make-instance 'clim-internals::image-pattern
                           :array
                           (clim-internals::convert-opticl-img
                            (funcall opticl-fun stream)))))))

;;; ideally you would speicalize on the message so that you don't clobber
;;; other people's handlers
(defmethod ip:handle-event ((tube ipw:api-call-tube) message (event ip:safe-queue-event))
  (declare (ignore tube))
  (call-next-method)
  (tube-event *invidious-client-frame* event))

(defmethod clim:handle-event ((frame invidious-client) (event tube-event))
  (let ((thread-monitor (get-frame-pane frame 'thread-monitor)))
    (ip.c:update-thread-information thread-monitor (event event))
    (redisplay-frame-pane frame thread-monitor :force-p t)))

(defmethod clim:handle-event ((frame invidious-client) (event redisplay-frame-pane-event))
  (redisplay-frame-pane frame (frame-pane event) :force-p t))

(defmethod clim:handle-event ((frame invidious-client) (event channel-detail-event))
  (let ((sourcing-cast-pane (get-frame-pane frame 'sourcing-cast-pane)))
    (setf (channel (channel-mould sourcing-cast-pane)) (channel event))
    (redisplay-frame-pane frame sourcing-cast-pane :force-p t)))

(defmethod clim:handle-event ((frame invidious-client) (event pagination-items-event))
  (add-items (pagination-mould event) (items event))
  (redisplay-frame-pane frame (cast-pane event) :force-p t))

(defmethod clim:frame-exit ((frame invidious-client))
  (dotimes (i (client:resource-loader-thread-count (client frame)))
    (infernal-pneumatics:defer-task
        infernal-pneumatic-wraps:*resource-loader-tube*
        #'infernal-pneumatics:stop-thread))
  (setf *threads-started-p* nil)
  (call-next-method))

;;; maybe this can be moved to an addable clim module in ipw?
(defmethod ip.c:display-thread-event (stream (event ipw:api-call-event))
  (surrounding-output-with-border (stream :shape :rectangle :ink +green+)
    (let ((time-delta (ip:time-delta event)))
      (format stream "CALLING ~a for ~,2F second~:P"
              (ipw:route-name event) (/ time-delta internal-time-units-per-second)))))
