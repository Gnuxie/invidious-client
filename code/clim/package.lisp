#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:invidious-client.clim
  (:use #:clim #:clim-lisp)
  (:local-nicknames
   (#:client #:invidious-client)
   (#:ip #:infernal-pneumatics)
   (#:ip.c #:infernal-pneumatics.clim)
   (#:ipw #:infernal-pneumatic-wraps))
  (:export
   #:start))
