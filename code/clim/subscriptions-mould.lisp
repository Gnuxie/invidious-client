#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

(defclass subscriptions-mould (pagination-mould)
  ())

(defmethod mould-pane ((mould subscriptions-mould) pane)
  ;; so i'm thinking that we have two columns, one for channels
  ;; one for the subscriptions
  ;; they should ovbiously not be in the same table.
  (call-next-method))
