#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

(defclass channel-mould (pagination-mould)
  ((%channel :initarg :channel :accessor channel)))

(defmethod mould-pane ((mould channel-mould) pane)
  (multiple-value-bind (x y) (stream-cursor-position pane)
    (declare (ignore y))
    (let ((channel (channel mould)))
      (when channel
        (present channel (presentation-type-of channel) :stream pane))
      (multiple-value-bind (x2 y2) (stream-cursor-position pane)
        (declare (ignore x2))
        (stream-set-cursor-position pane x y2))))
  (call-next-method))

(clim:define-presentation-method clim:present (channel (type client:detailed-channel)
                                                       (stream extended-output-stream)
                                                       view &key)
  (let ((pattern (client:data (client:author-banner channel))))
    (multiple-value-bind (x y) (stream-cursor-position stream)
      (draw-pattern* stream pattern x y))
    (clim:stream-increment-cursor-position stream 0 (pattern-height pattern)))
  (formatting-table (stream :x-spacing 5)
    (formatting-row (stream)
      (formatting-cell (stream)
        (let ((pattern (client:data (client:author-thumbnail channel))))
          (draw-pattern* stream pattern (pattern-width pattern) (pattern-height pattern))))
      (formatting-cell (stream :align-y :center)
        (write-string (client:author channel) stream)))))

(defclass channel-detail-event (invidious-client-event)
  ((%channel :initarg :channel :reader channel)))

(defgeneric make-channel-mould (mould-class &rest initargs)
  (:method (mould-class &rest initargs)
    (apply #'make-instance mould-class initargs)))
