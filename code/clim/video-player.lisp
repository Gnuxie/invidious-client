#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.clim)

;;; we're going to spawn a thraed and send the events
;;; from infernal pneumatics

(defclass playing-video-event (ip:safe-queue-event)
  ((%video :initarg :video :reader video)))

;;; I imagine this as purple with white text
;;; like the colour of the mpv logo?
(defmethod ip.c:display-thread-event (stream
                                      (event playing-video-event))
  (surrounding-output-with-border (stream :shape :rectangle :background +magenta4+)
    (let ((text (format nil "PLAYING ~a" (client:title (video event)))))
      (multiple-value-bind (x y) (stream-cursor-position stream)
        (draw-text* stream text x y :ink +white+ :align-y :top)))))

;;; an alternative is to spawn another thread on the tube
;;; when a video is played, and kill after it's finished playing?
;;; that helps if there's problems with error handling code.
(defun play-video (client video frame)
  (bt:make-thread
   (lambda ()
     (let ((play-event
             (make-instance 'playing-video-event
                            :video video)))
       (tube-event frame play-event)
       (client:play-video (client:video-player client) client video)
       (tube-event frame (make-instance 'ip:thread-stopped-event))))))
