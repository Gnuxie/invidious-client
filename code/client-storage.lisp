#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defvar *user-config-location*
  (uiop:subpathname (user-homedir-pathname) ".config/cl-invidious-client/client.obj"))

(defun load-client ()
  "The thinking atm is that the caller creates the default client rather than this."
  (when (uiop:file-exists-p *user-config-location*)
    (cl-store:restore *user-config-location*)))

(defun save-client (client)
  (ensure-directories-exist *user-config-location*)
  (cl-store:store client *user-config-location*))
