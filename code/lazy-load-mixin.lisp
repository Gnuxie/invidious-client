#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass lazy-resource ()
  ((%ready :initarg :ready
           :reader ready
           :type boolean :initform nil)))
