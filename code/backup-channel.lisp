#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defun video-id<-video-entry (item)
  (subseq
   (feeder:id item)
   (+ 6 (search "video:" (feeder:id item)))))

(defun author-id<-feed (feed)
  (subseq
   (feeder:id feed)
   (+ 8 (search "channel:" (feeder:id feed)))))

(defun simple-channel<-feed (feed)
  (make-instance 'simple-channel
                 :author (feeder:title feed)
                 :author-id (author-id<-feed feed)))

(defun ?channel-videos-from-atom (channel)
  "This provides a backup providing videos from a youtube atom feed
in the case that invidious is not working, which is quite often."
  (bb:alet ((raw-channel-feed
             (get-resource
              (format nil "https://www.youtube.com/feeds/videos.xml?channel_id=~a"
                      channel))))
    (let* ((channel-feed (first (feeder:parse-feed raw-channel-feed t)))
           (channel (simple-channel<-feed channel-feed)))
      (mapcar
       (lambda (content)
         (make-instance 'video
                        :video-id (video-id<-video-entry content)
                        :title (feeder:title content)
                        :published (feeder:published-on content)
                        :channel channel))

       (feeder:content channel-feed)))))

(defun channel-videos-with-atom-as-backup (client author-id &key thumbnail-loaded-callback)
  (bb:with-promise (resolve reject)
    (bb:alet ((videos (channel-videos
                       client author-id
                       :thumbnail-loaded-callback
                       thumbnail-loaded-callback)))
      (if videos
          (resolve videos)
          (bb:alet ((videos (?channel-videos-from-atom author-id)))
            (resolve videos))))))
