#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass client ()
  ((%invidious-sources :initarg :invidious-sources
                       :reader invidious-sources
                       :type list
                       :documentation "A list of autentication components
in order of precedence which we use to query the invidious api.")
   (%content-sources :initarg :content-sources
                     :reader content-sources
                     :type list
                     :documentation "A list of content-sources to use.")
   (%video-player :initarg :video-player
                  :reader video-player
                  :documentation "The video player to use.")

   (%subscriptions :initarg :subscriptions
                   :reader subscriptions
                   :initform (make-instance 'subscription-collection)
                   :documentation "this is just a single collection for now,
in future i would like there to be folders of subscribers that are clickable.")

   (%resource-loader-thread-count :initarg :resource-loader-thread-count
                                  :accessor resource-loader-thread-count
                                  :initform 1
                                  :type integer)))

(defun api-call (function client &rest arguments)
  (apply function (first (invidious-sources client)) arguments))

(defun %invoke-with-video-results (yield-thuncc &optional thumbnail-loaded-callback)
  (let ((no-thumbnail-results
         (bb:alet ((videos (funcall yield-thuncc)))
           (loop :for video :in videos
              :collect (make-video-from-result video)))))
    (bb:amap
     (lambda (result)
       (let ((thumbnail-hook
              (retrieve-data (thumbnail result))))
         (when thumbnail-loaded-callback
           (bb:attach thumbnail-hook thumbnail-loaded-callback)))
       result)
     no-thumbnail-results)
    no-thumbnail-results))

;;; is the blackbird style really better where the caller uses alet
;;; to get the main callback? I don't know, I think js style
;;; (lambda (error, data) ...) callbacks might still be better
;;; especially if they can be placed in an flet.
(defun youtube-search (client query &key thumbnail-loaded-callback)
  (%invoke-with-video-results
   (lambda ()
     (api-call #'api:search client :query query))
   thumbnail-loaded-callback))

(defun channel-videos (client channel &key thumbnail-loaded-callback)
  (%invoke-with-video-results
   (lambda ()
     (api-call #'api:channel-videos client channel))
   thumbnail-loaded-callback))

;;; TODO make some fancy macro that can do this automagically
;;; and maybe even infer whether to make simiple-channel or detailed-channel
(defun make-channel-from-channels-result (result)
  (make-instance 'detailed-channel
                 :author (jsown:val result "author")
                 :author-id (jsown:val result "authorId")
                 :author-url (jsown:val result "authorUrl")
                 :subscription-count (jsown:val result "subCount")
                 :author-thumbnail
                 (let ((thumbnail-jsown
                        (select-thumbnail (jsown:val result "authorThumbnails"))))
                   (make-instance 'thumbnail
                                  :url (jsown:val thumbnail-jsown "url")
                                  :width (jsown:val thumbnail-jsown "width")
                                  :height (jsown:val thumbnail-jsown "height")))
                 :author-banner
                 (let ((thumbnail-jsown
                        (select-thumbnail (jsown:val result "authorBanners"))))
                   (make-instance 'thumbnail
                                  :url (jsown:val thumbnail-jsown "url")
                                  :width (jsown:val thumbnail-jsown "width")
                                  :height (jsown:val thumbnail-jsown "height")))))

(defun channel-detail (client channel-id
                                   &key banner-load-callback
                                     thumbnail-load-callback)
  (let ((no-thumbnail-channel
         (bb:alet ((channel (api-call #'api:channels client channel-id)))
           (make-channel-from-channels-result channel))))
    (bb:attach no-thumbnail-channel
               (lambda (channel)
                 (let ((author-thumbnail-load-hook (retrieve-data (author-thumbnail channel)))
                       (author-banner-load-hook (retrieve-data (author-banner channel))))
                   (when thumbnail-load-callback
                     (bb:attach author-thumbnail-load-hook thumbnail-load-callback))
                   (when banner-load-callback
                     (bb:attach author-banner-load-hook banner-load-callback)))))
    no-thumbnail-channel))
