#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:invidious-client
  (:use #:cl)
  (:local-nicknames
   (#:api #:invidious-client.api)
   (#:feeder #:org.shirakumo.feeder)
   (#:ip #:infernal-pneumatics)
   (#:ipw #:infernal-pneumatic-wraps))
  (:export
   ;; channel
   ;;; simple-channel
   #:simple-channel
   #:author
   #:author-id
   ;;; detailed-channel
   #:detailed-channel
   #:author-thumbnail
   #:author-banner
   #:subscription-count
   ;; video
   ;;; video
   #:video
   #:title
   #:video-id
   #:published
   #:channel
   ;;; detailed-video
   #:detailed-video
   #:video-length
   #:view-count
   #:thumbnail
   ;;; thumbnail
   #:*default-thumbnail-data*
   #:thumbnail
   #:url
   #:width
   #:height
   #:data

   #:update-thumbnail-with-data
   #:retrieve-data
   #:select-thumbnail
   ;; content-source
   #:content-source
   #:make-video-url
   #:youtube
   ;; backup
   #:channel-videos-with-atom-as-backup
   ;; client
   #:client
   #:invidious-sources
   #:content-sources
   #:video-player
   #:subscription
   #:resource-loader-thread-count
   #:api-call
   #:youtube-search
   #:channel-videos
   #:channel-detail
   ;; subscription-collection
   #:subscription-collection
   #:subscribe
   #:subscribed-videos
   #:subscriptions
   ;; client storage
   #:*user-config-location*
   #:load-client
   #:save-client
   ;; video-player
   #:video-player
   #:play-video
   #:mpv
   #:play-video
   ))
