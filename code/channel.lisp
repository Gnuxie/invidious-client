#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client)

(defclass simple-channel ()
  ((%author :initarg :author :reader author :type string
            :documentation "This is the display name")
   (%author-id :initarg :author-id :reader author-id :type string)
   (%author-url :initarg :author-url :reader author-url
                :documentation "does anyone use author-url, i don't think it
should be included on the simple channel."))
  (:documentation "This is the minimum information that you can use
to represent a channel, this is given on all videos and playlists."))

;;; we need a new way of seperating thumbnails.
(defclass detailed-channel (simple-channel)
  ((%author-thumbnail :initarg :author-thumbnail :accessor author-thumbnail)
   (%author-banner :initarg :author-banner :accessor author-banner)
   (%subscription-count :initarg :subscription-count :reader subscription-count)))
