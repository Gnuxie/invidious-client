(asdf:defsystem #:invidious-client.api
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :serial t
  :depends-on ("invidious-api-description" "rest-api-description.drakma-generator"
                                           "jsown" "infernal-pneumatic-wraps")
  :components ((:file "package")
               (:file "build-package")))
