#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:invidious-client.build-api)

(dg:define-api "INVIDIOUS-CLIENT.API" "INVIDIOUS-API-DESCRIPTION"
  :generator ipw:infernal-pneumatic-drakma
  :wrapper-function wrapper-function)
