#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:invidious-client.build-api
  (:use #:cl)
  (:local-nicknames
   (#:dg #:rest-api-description.drakma-generator)
   (#:rad #:rest-api-description)
   (#:iad #:invidious-api-description)
   (#:ipw #:infernal-pneumatic-wraps)))

(in-package #:invidious-client.build-api)

(defun wrapper-function (config route request-form)
  (declare (ignore config route))
  `(multiple-value-bind (body status)
       ,request-form
     (declare (ignore status))
     (jsown:parse body)))

(dg:ensure-package '#:invidious-client.api)
