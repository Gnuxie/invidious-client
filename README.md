# invidious-client

# UPDATE 2021

Not sure what the status of the invidious web API is after the main instance went down, which instances support it
and whether authentication is required (The documentation doesn't seem to be up to date) so this will no longer work unless you can answer these questions yourself.

![](resource/example-screenshot.png)

This is an invidious-client in Common Lisp with a clim frontend, it's really bad and hacky atm

you will need following dependencies

```
https://gitlab.com/Gnuxie/infernal-pneumatics
https://gitlab.com/Gnuxie/rest-api-description
https://gitlab.com/Gnuxie/invidious-api-description
```

You will also need [feeder](http://github.com/Shinmera/feeder) which can be obtained from `http://github.com/Shinmera/feeder` or the shirakumo quicklisp distribution.



## usage

load the `invidious-client.clim` system and then start with `invidious-client.clim:start`.

## video player

The video player to use is specified in the `client` and it uses the protocol described in [video-player.lisp](https://gitlab.com/Gnuxie/invidious-client/-/blob/master/code/video-player.lisp). It should be very easy to use your own player, although I don't know how many players allow streaming from youtube-dl in the way mpv does.

## License

Artistic License 2.0

Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>
